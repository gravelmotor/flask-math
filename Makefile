 
NAME   := flask-math
TAG    := $$(git log -1 --pretty=%!H(MISSING))
IMG    := ${NAME}:${TAG}
LATEST := ${NAME}:latest
 
build:
  @docker build -t ${IMG} .
  @docker tag ${IMG} ${LATEST}
