from flask import Flask
from flask import request
import json
from random import random

from prometheus_client import make_wsgi_app
from werkzeug.middleware.dispatcher import DispatcherMiddleware
from werkzeug.serving import run_simple
from flask_prometheus_metrics import register_metrics

app = Flask(__name__)

@app.route("/")
def index():
    return "Test"

# provide app's version and deploy environment/config name to set a gauge metric
register_metrics(app, app_version="v0.1.2", app_config="staging")

# Plug metrics WSGI app to your main app with dispatcher
dispatcher = DispatcherMiddleware(app.wsgi_app, {"/metrics": make_wsgi_app()})

run_simple(hostname="localhost", port=5000, application=dispatcher)

# It will take two numbers and add them together.
@app.route('/add')
def add():
    t = json.loads(request.args.get('terms', ''))
    return {
        "value": t[0] + t[1]
        }

# It will take two real numbers and return the result of subtracting them.
@app.route('/subtract')
def subtract():
    t = json.loads(request.args.get('terms', ''))
    return {
        "value": t[0] - t[1]
        }

# Divide two numbers, return the result.
@app.route('/division')
def division():
    t = json.loads(request.args.get('terms', ''))
    print(t)
    return {
        "value": t[0] / t[1]
        }

# •Optional argument Count.
# •It will return by default 10 random numbers if Count provided return the amount of random numbers requested.
@app.route('/random')
def rand():
    count = json.loads(request.args.get("count", "10"))
    return {
        "value": [ random() for item in (["haha"] * count) ]
        }

# Provide prometheus metrics that you see fit.
#@app.route('/metrics')
#def metrics():
#    t = request.args.get('terms', '')

# 200 if the service is alive.
@app.route('/liveness')
def liveness():
    return "OK", 200

# 200 if service is ready to take requests.
# no dependencies so equiv with liveness
@app.route('/readiness')
def readiness():
    return "OK", 200

