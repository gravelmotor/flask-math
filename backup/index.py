from flask import Flask
from flask import request
import json
from random import random

app = Flask(__name__)

# It will take two numbers and add them together.
@app.route('/add')
def add():
    t = json.loads(request.args.get('terms', ''))
    return {
        "value": t[0] + t[1]
        }

# It will take two real numbers and return the result of subtracting them.
@app.route('/subtract')
def subtract():
    t = json.loads(request.args.get('terms', ''))
    return {
        "value": t[0] - t[1]
        }

# Divide two numbers, return the result.
@app.route('/division')
def division():
    t = json.loads(request.args.get('terms', ''))
    print(t)
    return {
        "value": t[0] / t[1]
        }

# •Optional argument Count.
# •It will return by default 10 random numbers if Count provided return the amount of random numbers requested.
@app.route('/random')
def rand():
    count = json.loads(request.args.get("count", "10"))
    return {
        "value": [ random() for item in (["haha"] * count) ]
        }

# Provide prometheus metrics that you see fit.
@app.route('/metrics')
def metrics():
    t = request.args.get('terms', '')


# 200 if the service is alive.
@app.route('/liveness')
def liveness():
    return "OK", 200


# 200 if service is ready to take requests.
# no dependencies so equiv with liveness
@app.route('/readiness')
def readiness():
    return "OK", 200

